﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnText : MonoBehaviour
{

    private bool _active = false, _animationTrigger = false;
    private float _curScale = 0;
    public GameObject TextObj;

    void Start()
    {
        TextObj.SetActive(_active);
    }
	
	void Update () {
	    
        TouchHandling();
        AnimateText();
	}

    private void TouchHandling()
    {
        foreach(var touch in Input.touches)
        {
            if (touch.phase == TouchPhase.Began) //hey this is awesome
            {
                var ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit)) ;
                if (hit.collider.tag == "Text")
                {
                    _active = !_active;
                    //reset Textobj size if it is not active
                    if (!_active) TextObj.transform.localScale = Vector3.zero;
                    //starts animation if textobj active
                    if(_active) _animationTrigger = true;

                    TextObj.SetActive(_active);
                }
            }
        }
    }
    //scale up text to make it look animated
    private void AnimateText()
    {
        if (_animationTrigger)
        {
            _curScale += Time.deltaTime;
            TextObj.transform.localScale = new Vector3(_curScale, _curScale, _curScale);
            if (_curScale >= 1)
            {
                _animationTrigger = false;
                _curScale = 0;
            }
        }
    }
}
