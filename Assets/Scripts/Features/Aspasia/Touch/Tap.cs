﻿using UnityEngine;
using UnityEngine.Events;

namespace Touch
{
	[System.Serializable]
	public class TapEvent : UnityEvent<Tap> {}
	
	[AddComponentMenu("Touch/Tap")]
	public class Tap : MonoBehaviour 
	{
		[SerializeField]
		public TapEvent m_OnTap;
		
		public void DoTap()
		{
			m_OnTap.Invoke(this);
		}
	}
}

