using UnityEngine;
using UnityEngine.Events;
using System.Collections;

namespace  Touch
{
	[System.Serializable]
	public class DragNDropEvent : UnityEvent<DragOperation> {}
	public class DragOperation
	{
		private Drag m_drag;
		private Drop m_target;
		private bool m_accepted;
		//------------------------------------------------------------------------------------------------------
		public GameObject draggedObject
		{
			get { return m_drag.gameObject; }
		}
		//------------------------------------------------------------------------------------------------------
		public GameObject source
		{
			get { return m_drag.source.gameObject; }
		}
		//------------------------------------------------------------------------------------------------------
		public GameObject target
		{
			get { return m_target.gameObject; }
		}
		//------------------------------------------------------------------------------------------------------
		public bool accepted
		{
			get { return m_accepted; }
		}
		//------------------------------------------------------------------------------------------------------
		public bool rejected
		{
			get { return !m_accepted; }
		}
		//------------------------------------------------------------------------------------------------------
		public DragOperation(Drag _drag, Drop _target)
		{
			m_drag = _drag;
			m_target = _target;
			m_accepted = false;
		}
		//------------------------------------------------------------------------------------------------------
		public void Accept()
		{
			if(!m_accepted)
			{
				// fire events at drop locations
				m_drag.source.Detached(m_drag.gameObject);
				m_target.Attached(m_drag.gameObject);
				
				// actually move the dragged object
				m_drag.DropAt(m_target);
				
				m_accepted = true;
			}
		}
		//------------------------------------------------------------------------------------------------------
		public void Reject()
		{
			if(!m_accepted)
			{
				// move dragged item back
				m_drag.ReturnToSource();
			}
		}
	}
}