﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

///////////////////////////////////////////////////////
// Drop
//-----------------------------------------------------
// A location where a dragable element can be dropped
///////////////////////////////////////////////////////

namespace Touch
{
	[System.Serializable]
	public class DropEvent : UnityEvent<GameObject, GameObject> {}
	
	[AddComponentMenu("Touch/Drop")]
    public class Drop : MonoBehaviour
    {
        [SerializeField]
        private DropEvent m_OnDetach;
        [SerializeField]
        private DropEvent m_OnAttach;

        private bool m_occupied = false;
        //------------------------------------------------------------------------------------------------------
        public bool occupied
        {
            get { return m_occupied; }
            set { m_occupied = value; }
        }
		//------------------------------------------------------------------------------------------------------
		public bool free
		{
			 get { return !m_occupied; }
			 set { m_occupied = !value; }
		}
        //------------------------------------------------------------------------------------------------------
        public void Detached(GameObject _drag)
        {
            m_OnDetach.Invoke(_drag, gameObject);
        }
        //------------------------------------------------------------------------------------------------------
        public void Attached(GameObject _drag)
        {
            m_OnAttach.Invoke(_drag, gameObject);
        }
    }
}