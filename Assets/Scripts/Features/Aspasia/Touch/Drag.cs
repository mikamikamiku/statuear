﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

namespace  Touch
{
	public class LiftEvent : UnityEvent<Drag> {}
	
	[AddComponentMenu("Touch/Drag")]
	public class Drag : MonoBehaviour 
	{
		[SerializeField]
		public DragNDropEvent OnDragNDrop;
		
		[SerializeField]
		private LiftEvent m_OnLift;
		
		private Drop m_source;
		private bool m_dragable = true;
		
		//------------------------------------------------------------------------------------------------------
		void Start()
		{
			m_source = transform.GetComponentInParent<Drop>();
			Debug.Assert(m_source, "GameObject with Drag component must be child of GameObject with Drop component");
		}
		//------------------------------------------------------------------------------------------------------
		///<Summary>If true the game object will not be dragable</summary>
		public bool dragable
		{
			get { return m_dragable;}
			set { m_dragable = value; }
		}
		//------------------------------------------------------------------------------------------------------
		///<Summary>If true the game object will not be dragable</summary>
		public bool fastened
		{
			get { return !m_dragable;}
			set { m_dragable = !value; }
		}
		//------------------------------------------------------------------------------------------------------
		// the drop location draged from
		public Drop source
		{
			get { return m_source; }
		}
		//------------------------------------------------------------------------------------------------------
		public void LiftBy(Transform _socket)
		{
			if(m_dragable)
			{
				m_source = transform.GetComponentInParent<Drop>();
				Debug.Assert(m_source, "Invalid parent for GameObject with Drag component. Drops should not be removed as long as Drags are attached to it");
				// attach drag to dragger
				transform.SetParent(_socket, false);
				if(m_OnLift != null)
				{
					m_OnLift.Invoke(this);
				}
			}		
		}
		//------------------------------------------------------------------------------------------------------
		// don't yet drop but wait for accepance by the user
		public void TryDropAt(Drop _target)
		{
			DragOperation dnd = new DragOperation(this, _target);
			if(OnDragNDrop != null)
			{
				OnDragNDrop.Invoke(dnd);
			}
			else
			{
				dnd.Reject();
			}
		}
		//------------------------------------------------------------------------------------------------------
		// attach to target drop location
		public void DropAt(Drop _target)
		{
			if(_target)
			{
				transform.SetParent(_target.transform, false);
				m_source = _target;
			}
			else
			{
				Debug.LogError("Unallocated drop destination");
				ReturnToSource();
			}
		}
		//------------------------------------------------------------------------------------------------------
		// drag and drop unsuccessful -> return to origin
		public void ReturnToSource()
		{
			Debug.Assert(m_source, "No source for drag found. Drops should not be destroyed on detached.");
			transform.SetParent(m_source.transform, false);
		}
	}
}