﻿using UnityEngine;

namespace Touch
{
	[AddComponentMenu("")]
	public class Dragger : MonoBehaviour 
	{
		private bool m_occupied = false;
		private Drag m_attachment;
		private Vector3 m_destinationPosition;
		//------------------------------------------------------------------------------------------------------	
		public bool occupied
		{
			get 
			{
				return m_occupied;
			}
		}
		//------------------------------------------------------------------------------------------------------
		public Vector3 position
		{
			get {return transform.position;}
			set { m_destinationPosition = transform.position; }
		}
		//------------------------------------------------------------------------------------------------------
		void Start()
		{
			m_destinationPosition = transform.position;
		}
		//------------------------------------------------------------------------------------------------------
		// place in socket
		public bool Drag(Drag _attachment)
		{
			if(!occupied)
			{
				transform.localScale = _attachment.source.transform.lossyScale;
				_attachment.LiftBy(transform);
				m_attachment = _attachment;
				m_occupied = true;
			}
			return false;
		}
		//------------------------------------------------------------------------------------------------------
		// return to origin
		public void LooseAttachment()
		{
			if(m_occupied)
			{
				m_attachment.ReturnToSource();
				m_occupied = false;
			}
		}
		//------------------------------------------------------------------------------------------------------
		// drop at new location
		public void DropAttachment(Drop _drop)
		{
			if(m_occupied)
			{
				m_attachment.TryDropAt(_drop);
				m_occupied = false;
			}
		}
		//------------------------------------------------------------------------------------------------------
		void Update()
		{
			// this could be smoothed
			//transform.position = m_destinationPosition;
		}
	}
}
