﻿using UnityEngine;

// the drag n drop interface would be nice in the asset store, right?
namespace Touch
{
	[AddComponentMenu("")]
	public class TouchPlane : MonoBehaviour
	{
		[SerializeField]
		private Camera m_camera;
		[SerializeField]
		private LayerMask m_mask;
		
		private enum Phase
		{
			Nothing,
			Began,
			Ended
		}
		private Phase m_phase = Phase.Nothing;

		private Dragger m_finger; // FUTURE WORK: multitouch
		//------------------------------------------------------------------------------------------------------
		void Start()
		{
			GameObject fingerObject = new GameObject("Finger");
			fingerObject.transform.SetParent(transform, false);
			m_finger = fingerObject.AddComponent<Dragger>();
		}
		//------------------------------------------------------------------------------------------------------
		// Update is called once per frame
		void Update () 
		{
			// Touch input
			Vector3 mouseCameraSpace = new Vector3();
			mouseCameraSpace.x	= (m_camera.pixelWidth / (float) Camera.main.pixelWidth) * Input.mousePosition.x;
			mouseCameraSpace.y = (m_camera.pixelHeight / (float) Camera.main.pixelHeight) * Input.mousePosition.y;
			Ray touchRay = m_camera.ScreenPointToRay(mouseCameraSpace);
			RaycastHit[] touchHits = Physics.RaycastAll(touchRay, 100.0f, m_mask);
			
			// finger was put down
			if(Input.GetMouseButtonDown(0) && m_phase != Phase.Began)
			{
				m_phase = Phase.Began;
				// check for touched objects
				foreach(RaycastHit hit in touchHits)
				{
					Debug.Log("Hiz");
					// select objects with tap component
					Tap tapComponent = hit.collider.GetComponent<Tap>();
					if(tapComponent != null)
					{
						tapComponent.DoTap();
					}
					
					// select objects with drag component
					Drag dragComponent = hit.collider.GetComponent<Drag>();
					if(dragComponent != null)
					{
						// some dragable items might currently be immovable
						if(dragComponent.dragable)
						{
							m_finger.Drag(dragComponent);
							PositionFinger(touchRay);
						}
					}
				}
			}
			// finger was released
			else if(Input.GetMouseButtonUp(0) && m_phase == Phase.Began)
			{
				m_phase = Phase.Ended;
				foreach(RaycastHit hit in touchHits)
				{
					// select drop destinations in hits
					Drop dropComponent = hit.collider.GetComponent<Drop>();
					if(dropComponent != null)
					{
						// some drop locations may already be occupied
						if(!dropComponent.occupied)
						{
							// drop at the destination
							m_finger.DropAttachment(dropComponent);
						}
					}
				}
				// loose any attachments if no drop was found
				m_finger.LooseAttachment();
			}
			// finger was moved
			else if(m_phase == Phase.Began)
			{
				PositionFinger(touchRay);
			}
		}
		//------------------------------------------------------------------------------------------------------
		private void PositionFinger(Ray _ray)
		{
			// generate touch plane (we can't assume it is constant because user could move the plane)
			Vector3 planePosition = transform.position;
			Plane plane = new Plane(m_camera.transform.forward, transform.position);
			float distance = 0;
			
			// intersect ray
			if (plane.Raycast(_ray, out distance))
			{
				m_finger.transform.position = _ray.GetPoint(distance);
			}
		}
	}
}