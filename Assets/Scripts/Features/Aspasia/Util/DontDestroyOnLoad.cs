﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Util
{
	public class DontDestroyOnLoad : MonoBehaviour {
		void Awake() {
			DontDestroyOnLoad(gameObject);
		}
	}

}
