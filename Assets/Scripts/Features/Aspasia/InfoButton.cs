﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfoButton : MonoBehaviour {
	public InfoCanvas m_Canvas;
	public GameObject m_ContentPrefab;

	public void OnTap()
	{
		m_Canvas.Show(m_ContentPrefab);
	}
}
