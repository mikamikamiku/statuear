﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Aspasia: MonoBehaviour {

	public enum State 
	{
		None,
		InfoBoxes,
		HalfCircle,
		ProtestSign,
		Exit
	}

	private State m_State = State.None;
	public Text m_SceneTitle;

	// Use this for initialization
	void Start () {
		NextScene();
	}
	
	public void NextScene()
	{
		string SceneName = string.Empty;
		// Unload old sub scene
		if( m_State != State.None)
		{
			SceneName = GetSceneName();
			SceneManager.UnloadSceneAsync(SceneName);
		}
		// Progress to next scene
		m_State++;
		// If this was the last scene, return to main menu
		if(m_State == State.Exit)
		{
			SceneManager.LoadScene("MainMenu");
			return;
		}
		// Load the next scene into the master scene, that still is loaded
		SceneName = GetSceneName();
		SceneManager.LoadSceneAsync(SceneName, LoadSceneMode.Additive);
		m_SceneTitle.text = GetSceneTitle();
	}

	public string GetSceneName()
	{
		switch (m_State)
		{
			case State.InfoBoxes:
				return "Info Boxes";
			case State.HalfCircle:
				return "Half Circle";
			case State.ProtestSign:
				return "Protest Sign";
			case State.Exit:
				return "MainMenu";
			default:
				return string.Empty;
		}
	}

	// Scene title is shown to the user
	public string GetSceneTitle()
	{
		switch (m_State)
		{
			case State.InfoBoxes:
				return "Infoboxen";
			case State.HalfCircle:
				return "Halbkreis der Begleiterinnen";
			case State.ProtestSign:
				return "Protestschild";
			default:
				return string.Empty;
		}
	}

	public State GetState()
	{
		return m_State;
	}
}
