﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfoCanvas : MonoBehaviour {

	public GameObject m_ContentPanel;
	private GameObject m_Content = null;

	public void Show(GameObject _ContentPrefab)
	{
		if(m_Content == null)
		{
			gameObject.SetActive(true);
			m_Content = Instantiate<GameObject>(_ContentPrefab);
			m_Content.gameObject.transform.SetParent(m_ContentPanel.transform, false);
		}
	}

	public void Hide()
	{
		Destroy(m_Content);
		m_Content = null;
		gameObject.SetActive(false);
	}
}
