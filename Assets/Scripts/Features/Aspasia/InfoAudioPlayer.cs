﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfoAudioPlayer : MonoBehaviour
{
    public GameObject m_PlayButton;
    public GameObject m_PauseButton;
    public AudioManager m_AudioManager;

    public void OnPlay()
    {
        m_PlayButton.SetActive(false);
        m_PauseButton.SetActive(true);
        m_AudioManager.PlayAudio();
    }

    public void OnPause()
    {
        m_PlayButton.SetActive(true);
        m_PauseButton.SetActive(false);
        m_AudioManager.PauseAudio();
    }

    void Update()
    {
        if(m_AudioManager.Status == AudioStatus.Ended)
        {
            m_PlayButton.SetActive(true);
            m_PauseButton.SetActive(false);
        }
    }
}