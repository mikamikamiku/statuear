﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Editable : MonoBehaviour {
    abstract public void OnEdit();
}
