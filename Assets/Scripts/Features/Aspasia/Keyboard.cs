﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Keyboard : MonoBehaviour {
	private TouchScreenKeyboard m_Keyboard = null;
	private string m_Text = string.Empty;
	private bool m_Done = false;

	void OnEnable()
	{
		m_Text = string.Empty;
		m_Keyboard = null;
		m_Done = false;
	}

	void OnDisable()
	{
		m_Text = string.Empty;
		m_Keyboard = null;
		m_Done = false;
	}

	public void Open(string _Text)
	{
		gameObject.SetActive(true);
		m_Text = _Text;
	}

	public bool IsOpened()
	{
		return string.IsNullOrEmpty(m_Text) == false;
	}

	public bool IsDone()
	{
		return m_Done;
	}

	public string GetText()
	{
		return m_Text;
	}

	public void Close()
	{
		gameObject.SetActive(false);
	}

	void OnGUI()
	{
		if(string.IsNullOrEmpty(m_Text) == false && m_Keyboard == null)
		{
			m_Keyboard = TouchScreenKeyboard.Open(m_Text, TouchScreenKeyboardType.Default, true, true, false, false);
		}
		
		if(m_Keyboard != null)
		{
			m_Text = m_Keyboard.text;
			m_Done = m_Keyboard.done;
		}
	}
}
