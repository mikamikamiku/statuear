﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// There should only be one editor in the scene
public class Editor : MonoBehaviour {
    public GameObject EditorButton;
    public GameObject EditButton;
	public GameObject DoneButton;

	public enum State {
		Inactive,
		SelectAction,
		EditObjects,
	}
	private State m_State = State.Inactive;

	// Use this for initialization
	void Start () {
		
	}

    public State GetState()
    {
        return m_State;
    }

	public void Activate()
	{
		if(m_State == State.Inactive)
		{
			m_State = State.SelectAction;
			ShowMainMenu(true);
		}
		else
		{
			m_State = State.Inactive;
			ShowMainMenu(false);
			ShowDone(false);
		}
	}

	private void ShowMainMenu(bool _Show)
	{
		EditButton.active = _Show;
	}

	private void ShowDone(bool _Show)
	{
		DoneButton.SetActive(_Show);
        EditorButton.SetActive(!_Show);
	}

	public void OnDone()
	{
		ShowDone(false);
		ShowMainMenu(true);
		m_State = State.SelectAction;
	}

	public void OnEdit()
	{
		ShowMainMenu(false);
		ShowDone(true);
		m_State = State.EditObjects;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
