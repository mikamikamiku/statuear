﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movable : MonoBehaviour {
    private Mover m_mover;

	// Use this for initialization
	void Start () {
        m_mover = GameObject.FindObjectOfType<Mover>();
	}

	// Update is called once per frame
	void Update () {
		
	}

    public void OnMove()
    {
        m_mover.AttachOrDetach(this);
    }
}
