﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Editable), typeof(Movable))]
public class AspasiaObject : MonoBehaviour {

    private Editable m_editable;
    private Movable m_movable;
    private Editor m_editor;

	// Use this for initialization
	void Start () {
        m_editable = GetComponent<Editable>();
        m_movable = GetComponent<Movable>();
        m_editor = GameObject.FindObjectOfType<Editor>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    // Object was clicked on or tapped --> depending on state, decide what to do
    public void OnTap()
    {
        switch (m_editor.GetState())
        {
            case Editor.State.EditObjects:
                m_editable.OnEdit();
                break;
            default:
                break;
        }
    }
}
