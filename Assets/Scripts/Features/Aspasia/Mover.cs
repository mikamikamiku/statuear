﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// A mover can move stuff around, there shall only be one
public class Mover : MonoBehaviour {
    private Vector3 m_previousPosition;
    private Quaternion m_previousRotation;
    private Movable m_attachedMovable = null;
    private Editor m_editor = null;

	// Use this for initialization
	void Start () {
	}

    // Update is called once per frame
    void Update() {
        //if (m_editor == null)
        //{
        //    m_editor = GameObject.FindObjectOfType<Editor>();
        //}
        //if (m_attachedMovable != null && m_editor.GetState() != Editor.State.MoveObjects)
        //{
        //    m_attachedMovable.gameObject.transform.position = m_previousPosition;
        //    m_attachedMovable.gameObject.transform.rotation = m_previousRotation;
        //    m_attachedMovable.gameObject.transform.parent = null;

        //    m_attachedMovable = null;
        //}
    }

    // Attach, if mover empty and detach if object is attached
    public void AttachOrDetach(Movable movable)
    {
        if (m_attachedMovable == null)
        {
            m_previousPosition = movable.gameObject.transform.position;
            m_previousRotation = movable.gameObject.transform.rotation;
            movable.gameObject.transform.SetParent(gameObject.transform, true);
            m_attachedMovable = movable;
        }
        else if (m_attachedMovable == movable)
        {
            m_attachedMovable.gameObject.transform.SetParent(null);
            m_attachedMovable = null;
        }
    }
}
