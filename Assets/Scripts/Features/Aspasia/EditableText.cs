﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Touch;

public class EditableText : Editable {

	public Text TextField;
	public Keyboard _Keyboard;
	private bool _bOpenKeyboard = false;

	// Use this for initialization
	void Start () 
	{
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(_Keyboard.IsOpened())
		{
			TextField.text = _Keyboard.GetText();
			if (_Keyboard.IsDone())
			{
				_Keyboard.Close();
			}
		}
	}

	public override void OnEdit()
	{
		_Keyboard.Open(TextField.text);
	}
}
