﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts
{
    public class Painter3D : MonoBehaviour
    {
        public Transform Camera;
        private LineRenderer _currentLR;
        private List<Transform> _lrList;
        private Vector3 _lastBrushPosition;
        private Vector3 _brushPosition;
        private bool _markerIsVisible = false;
        public string debugstring = "default";

        public Transform Drawing_Wand;

        void Awake()
        {
            GlobalData.Instance.Painter = this;
            Debug.Log("Added myself to globaldata");
            
        }

        void Start() {
            _lrList = new List<Transform>();
        }
	
        void Update () {
            DrawLine();
            Debug.Log(GlobalData.Instance.Painter.debugstring);
        }

        void OnGUI()
        {
            GUI.Label(new Rect(0, 0, 100, 100), _markerIsVisible.ToString());
            GUI.Label(new Rect(0, 100, 100, 100), debugstring);
        }

        public void SetMarkerIsVisible(bool b)
        {
            _markerIsVisible = b;
        }

        private void DrawLine()
        {
            if (_markerIsVisible)
            {
                _brushPosition = Drawing_Wand.position;

                var dist = Vector3.Distance(_lastBrushPosition, _brushPosition);
                if (dist > 0.1)
                {
                    _currentLR.positionCount++;
                    _currentLR.SetPosition(_currentLR.positionCount - 1, _brushPosition);
                    _lastBrushPosition = _brushPosition;
                }
            }
        }

        public void CreateLineRendererObject()
        {
            var obj = new GameObject("lrObj");
            obj.transform.position = Drawing_Wand.position;
            obj.transform.parent = transform;
            _currentLR = obj.AddComponent<LineRenderer>();
            _currentLR.material = new Material(Shader.Find("Standard"));
            _currentLR.material.color = Color.black;
            _currentLR.startWidth = 0.1f;
            _currentLR.endWidth = 0.1f;
            _currentLR.positionCount = 0;
            _lrList.Add(obj.transform);
        }

        /*private Vector3 GetFinger3DCoords()
    {
        var fingerPos = (Vector3)Input.touches[0].position;
        fingerPos.z = 5; //custom distance to screen plane. 5 seems to be a good parameter
        return Camera.GetComponentInChildren<Camera>().ScreenToWorldPoint(fingerPos);
    }*/
    }
}
