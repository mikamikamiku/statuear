﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class AudioManager : MonoBehaviour
{
    private AudioSource _source;
    public AudioStatus Status;

    public float _timer;

	void Awake ()
	{
	    _source = GetComponent<AudioSource>();
        Status = AudioStatus.Paused; //init as paused
    }

    void Update()
    {
        if (Status == AudioStatus.Playing)
        {
            _timer += Time.deltaTime;
        }

        if (_timer >= _source.clip.length)
        {
            Status = AudioStatus.Ended;
            _timer = 0;
        }
    }

    void OnGUI()
    {
        GUI.Label(new Rect(0,0,200,200), _timer.ToString());
    }

    public void SetAudioClip(AudioClip clip)
    {
        _source.clip = clip;
        Status = AudioStatus.Paused;
    }

    public void StopAudio()
    {
        _source.Stop();
        Status = AudioStatus.Stopped;
    }

    public void PauseAudio()
    {
        _source.Pause();
        Status = AudioStatus.Paused;
    }

    public void PlayAudio()
    {
       _source.Play();
        Status = AudioStatus.Playing;
    }
}

public enum AudioStatus
{
    Playing,
    Paused,
    Stopped,
    Ended
}
