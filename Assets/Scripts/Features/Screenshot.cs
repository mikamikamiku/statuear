﻿using System;
using UnityEngine;

public  static class Screenshot {

    private static int _index = 0;

    public static void MakeScreenshot()
    {
        //TODO try to find a more useful path
        //Application.persistentDataPath would work, but picture only shows up after reboot of device..so not very useful
        ScreenCapture.CaptureScreenshot(Application.dataPath + "\\Screenshots\\" + "screenshot" + _index +  ".jpg"); 
        _index++;
        Debug.Log("Screenshot done");
    }
}
