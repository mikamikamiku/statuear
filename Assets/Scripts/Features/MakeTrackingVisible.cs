﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Scripts.Event;

public class MakeTrackingVisible : MonoBehaviour {

	public GameObject MarkerMarkingObj;
	//private GameObject _obj;
	private TrackableEventHandler _tracking;
	//private LineRenderer _lr;

	void Start () {
		//_obj = new GameObject("LineRendererObj");
		//_obj.transform.position = transform.position;
		//_obj.transform.parent = transform;
		_tracking = GetComponent<TrackableEventHandler>();
		//InitLineRenderer();
	}
	
	void Update () {

		if(_tracking.Tracked){
			MarkerMarkingObj.SetActive(true);
			//_obj.SetActive(true);
		}else{
			MarkerMarkingObj.SetActive(false);
			//_obj.SetActive(false);
		}

	}
	//linerenderer not compatible with vuforia? not sure here..
	/*private void InitLineRenderer(){
		_lr = _obj.AddComponent<LineRenderer>();
		var mat = new Material(Shader.Find("Standard"));
		mat.color = Color.red;
		_lr.material = mat;
		_lr.numCapVertices = 10;
		_lr.numCornerVertices = 10;
		_lr.startWidth = 0.1f;
		_lr.endWidth = 0.1f;
		_lr.positionCount = 5;
		var root = transform.position;
		var x = root.x;
		var y = root.y;
		var z = root.z;
		_lr.SetPosition(0, root + new Vector3(0.5f, 0.1f, 0.5f));
		_lr.SetPosition(1, root + new Vector3(0.5f, 0.1f, -0.5f));
		_lr.SetPosition(2, root + new Vector3(-0.5f, 0.1f, -0.5f));
		_lr.SetPosition(3, root + new Vector3(-0.5f, 0.1f, 0.5f));
		_lr.SetPosition(4, root + new Vector3(0.5f, 0.1f, 0.5f));
	}*/
}
