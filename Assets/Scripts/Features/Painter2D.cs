﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Painter2D : MonoBehaviour
{
    private InputState _currentInputState = InputState.NotTouching;
    private InputState _lastInputState = InputState.NotTouching;
    private Vector3 _inputWorldPosition;
    private Vector3 _lastInputWorldPosition;
    private Vector3 _hitToCamera;
    private LineRenderer _currentLR;
    private List<Transform> _lrList;

    public Texture TEX;


    void Start () {
		_lrList = new List<Transform>();
	}
	
	void Update () {

        InputHandler();

        DrawLine();

        //TESTS
	    if (Input.GetKeyDown(KeyCode.Space))
	    {
	        DeleteLatestLine();
	    }
    }

    private void InputHandler()
    {
        _lastInputState = _currentInputState;

        Mobile();
        Computer();
    }

    private void DrawLine()
    {
        //on mouse/finger down
        if (_currentInputState == InputState.Touching && _lastInputState == InputState.NotTouching)
        {
            CreateLineRendererObject();
        }

        if (_currentInputState == InputState.Touching)
        {
            var dist = Vector3.Distance(_lastInputWorldPosition, _inputWorldPosition);
            if (dist > 0.05f)
            {
                _currentLR.positionCount++;
                _currentLR.SetPosition(_currentLR.positionCount - 1, _inputWorldPosition);
                _lastInputWorldPosition = _inputWorldPosition;
            }
        }
    }

    public void DeleteLatestLine()
    {
        if (_lrList.Count <= 0) return;
        GameObject.Destroy(_lrList[_lrList.Count - 1].gameObject);
        _lrList.RemoveAt(_lrList.Count - 1);
    }

    public void ClearPainting()
    {
        foreach (var lr in _lrList)
        {
            GameObject.Destroy(lr.gameObject);
        }
        _lrList.Clear();
    }


    private void CreateLineRendererObject()
    {
        var obj = new GameObject("lrObj");
        obj.transform.position = transform.InverseTransformPoint(_inputWorldPosition);
        obj.transform.parent = transform;
        obj.transform.position = Vector3.zero;
        _currentLR = obj.AddComponent<LineRenderer>();
        _currentLR.material = new Material(Shader.Find("Mobile/VertexLit"));
        _currentLR.useWorldSpace = true; //TODO maybe fix this sometime.. not sure what is going on here?
        //_currentLR.material.color = Color.black;
        _currentLR.useWorldSpace = false; //false is what I want, but it's not working
        _currentLR.startWidth = 0.02f;
        _currentLR.endWidth = 0.02f;
        _currentLR.positionCount = 0;
        _lrList.Add(obj.transform);
    }

    private Vector3 HitToCamera(Vector3 hitpos)
    {
        return (Camera.main.transform.position - hitpos).normalized;
    }

    private void Computer() //for testing only
    {
        if (Input.GetMouseButton(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform.tag == "DrawingPlane")
                {
                    _currentInputState = InputState.Touching;
                    _inputWorldPosition = hit.point + HitToCamera(hit.point) * 0.1f;
                }
                else
                {
                    _currentInputState = InputState.NotTouching;
                }
            }
        }
        else
        {
            _currentInputState = InputState.NotTouching;
        }
    }

    private void Mobile()
    {
        if (Input.touchCount > 0)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform.tag == "DrawingPlane")
                {
                    _currentInputState = InputState.Touching;
                    _inputWorldPosition = hit.point;// + HitToCamera(hit.point) * 0.1f;
                }
                else
                {
                    _currentInputState = InputState.NotTouching;
                }
            }
        }
        else
        {
            _currentInputState = InputState.NotTouching;
        }
    }

    private enum InputState
    {
        Touching,
        NotTouching
    }
}


