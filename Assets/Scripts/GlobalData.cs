﻿using System;
using System.Collections.Generic;
using Assets.Scripts;

public class GlobalData
{

    private static GlobalData instance;

    private List<Rotatable> RotatableList;
    public Painter3D Painter;

    private GlobalData() { }

    //lazy instantiation
    public static GlobalData Instance
    {
        get
        {
            if (instance == null)
            {
                instance = new GlobalData();
            }
            return instance;
        }
    }

    public List<Rotatable> GetRotatableList()
    {
        if (RotatableList == null)
        {
            RotatableList = new List<Rotatable>();
        }
        return RotatableList;
    }

    public void AddRotatableToList(Rotatable rot)
    {
        if (RotatableList == null)
        {
            RotatableList = new List<Rotatable>();
        }
        RotatableList.Add(rot);
    }
}

