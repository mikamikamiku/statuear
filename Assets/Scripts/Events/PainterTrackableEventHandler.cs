﻿using UnityEngine;
using Vuforia;

namespace Assets.Scripts
{
    public class PainterTrackableEventHandler : MonoBehaviour, ITrackableEventHandler
    {
        public Transform Painter3DTransform;
        private TrackableBehaviour mTrackableBehaviour;

        private Painter3D _painter;
        // Use this for initialization
        void Start ()
        {
            mTrackableBehaviour = GetComponent<TrackableBehaviour>();
            if (mTrackableBehaviour)
            {
                mTrackableBehaviour.RegisterTrackableEventHandler(this);
            }
            else
            {
                Debug.Log("PainterTrackableBehaviour has some issues, check it out pls");
            }
            _painter = GlobalData.Instance.Painter;
            _painter.debugstring = "painterinstance found";
        }
	
        public void OnTrackableStateChanged(TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
        {
            if (newStatus == TrackableBehaviour.Status.DETECTED ||
                newStatus == TrackableBehaviour.Status.TRACKED ||
                newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
            {
                OnTrackingFound();
            }
            else
            {
                OnTrackingLost();
            }
        }

        private void OnTrackingFound()
        {
            Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
            foreach (Renderer component in rendererComponents)
            {
                component.enabled = true;
            }

            if (_painter != null)
            {
                _painter.debugstring = "activated";
                _painter.SetMarkerIsVisible(true);
                _painter.CreateLineRendererObject();
            }

        }

        private void OnTrackingLost()
        {
            Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
            foreach (Renderer component in rendererComponents)
            {
                component.enabled = false;
            }

            if (_painter != null)
            {
                _painter.debugstring = "deactivated";
                _painter.SetMarkerIsVisible(false);
            }

        }
    }
}
