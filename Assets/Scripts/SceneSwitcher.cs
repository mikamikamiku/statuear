﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneSwitcher : MonoBehaviour {

    public void LoadScene(int x)
    {
        SceneManager.LoadScene(x);
    }
}
