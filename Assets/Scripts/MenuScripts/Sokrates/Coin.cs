﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    private Vector3 _coinBagPos;
    private float _speed = 1;


    public void Init(Vector3 goal)
    {
        _coinBagPos = goal;
        transform.localScale = Vector3.one;
        transform.eulerAngles = new Vector3(0,0,0);
        transform.Rotate(Vector3.right, 90);
        transform.localPosition += new Vector3(0,1300,0);
        transform.parent.parent.GetComponent<SokratesUIHandler>().AddCoin();
    }

	void Update ()
	{
	    transform.Translate(Vector3.down * _speed * Time.deltaTime);
	    _speed += Time.deltaTime * 3f;

	    if (Vector3.Distance(transform.position, _coinBagPos) < 0.5f)
	    {
	        //add coin was here, but thats buggy if I switch ui fuck no time
            Destroy(gameObject);
	    }
	}
}
