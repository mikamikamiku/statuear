﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Event;
using UnityEngine;
using UnityEngine.UI;

public class SokratesUIHandler : MonoBehaviour
{
    //may god forgive me for my sins
    #region variable init

    public Transform SOUNDBUTTONS;
    public Transform SOKRATESZITATE;
    public Transform STEP_3;
    public Transform STEP_4;
    public Transform STEP_5;
    public Transform QUIZ_TRANSFORM;
    public Transform SOKRATES_END;
    public GameObject CoinPrefab;
    
    public Transform COIN_NUMBER, QUESTION_FIELD, A0, A1, A2, A3, QTRUE, QFALSE;
    public Transform SOKRATESMARKER;
    public Transform SPEECHBUBBLETEXT;
    public Transform INPUT_FIELD;
    public AudioClip AUDIOCLIP;
    public AudioClip AUDIOCLIP2;

    public Transform SwitchingImage;
    public Sprite img1;
    public Sprite img2;
    public Sprite img3;
    public Sprite img4;
    public Sprite img5;

    private AudioManager _audioManager;
    private TrackableEventHandler _trackableEventHandler;

    private int _coins = 0;
    private QuizQuestion _currentQuizQuestion;
    public SokratesProgress _progress;

    private bool _changeUI = false;

    private bool _quiz = false, _quizCont = true;
    private int _quizIndex = 0;
    private List<QuizQuestion> _qqList;
    private List<float> _timerList;
    private bool _switchPicture = false;
    private List<Sprite> _texList;

    #endregion

    void Start () {
	    _trackableEventHandler = SOKRATESMARKER.GetComponent<TrackableEventHandler>();

        //testing stuff
	    _audioManager = SOKRATESMARKER.GetComponent<AudioManager>();
        _audioManager.SetAudioClip(AUDIOCLIP);

        _progress = SokratesProgress.Step0; //init at 0, not sure if necessary

        //tests
        _progress = SokratesProgress.Step0;
	    _changeUI = true;

        //init quizquestions
        InitQQList();

        //init timerList
        _timerList = new List<float>();
        _timerList.Add(78);
        //_timerList.Add(2);
        //_timerList.Add(3);
        //_timerList.Add(5);
        //_timerList.Add(8);

        _texList = new List<Sprite>();
        _texList.Add(null);//OHNO :D
        _texList.Add(img1);
        //_texList.Add(img2);
        //_texList.Add(img3);
        //_texList.Add(img4);
        //_texList.Add(img5);
    }
	
	void Update () {

        CheckProgressConditions();
	    UIHandler();

	    if (_quiz && _quizCont)
	    {
	        QuizLoop();
	    }

        //
	    if (_progress == SokratesProgress.Step1)
	    {
	        ChangePicturesToSoundfile();
	    }

        //manual skipping through stages! DEBUG ONLY : >
	    if ((Input.touchCount == 4 && Input.touches[3].phase == TouchPhase.Began) || Input.GetKeyDown(KeyCode.Space))
	    {
	        _progress++;
	        _changeUI = true;
	    }
	}

    private void ChangePicturesToSoundfile()
    {
        foreach (var timer in _timerList)
        {
            if (_audioManager._timer > timer)
            {
                _switchPicture = true;
            }
        }

        if (_switchPicture)
        {
            _texList.RemoveAt(0);
            _timerList.RemoveAt(0); //dont delete while working on list
            _switchPicture = false;
            var img = SwitchingImage.GetComponent<Image>();
            img.sprite = _texList[0];
            img.preserveAspect = true;
        }
    }

    private void QuizLoop()
    {
        _quizCont = false;

        if (_quizIndex == 0)
        {
            StartQuiz(_qqList[0]);
        }else if (_quizIndex == 1)
        {
            StartQuiz(_qqList[1]);
        }
        else if (_quizIndex == 2)
        {
            StartQuiz(_qqList[2]);
        }
        else if (_quizIndex == 3)
        {
            StartQuiz(_qqList[3]);
        }
        else if (_quizIndex == 4)
        {
            StartQuiz(_qqList[4]);
        }
        else if (_quizIndex == 5)
        {
            SwitchUITo(SOKRATES_END);
            SOKRATES_END.GetComponentInChildren<Text>().text = "Glückwunsch: " + _coins + "/5 Münzen erhalten";
        }
    }

    private void InitQQList() 
    {
        //i am so sorry
        //see what I did there? init qq :>
        _qqList = new List<QuizQuestion>();
        
        var question = "Aus welchen Gründen wurde Sokrates angeklagt?";
        var list0 = new List<string>();
        list0.Add("Landesverrat");
        list0.Add("Veruntreuung von Staatsbesitz");
        list0.Add("Gottlosigkeit, Verderben der Jugend");
        list0.Add("Kriegsdienstverweigerung");
        _qqList.Add(new QuizQuestion(question, list0, 2));

        question = "Wie wurde Sokrates\' Todesstrafe vollstreckt?";
        var list1 = new List<string>();
        list1.Add("durch einen Gifttrank");
        list1.Add("durch Folter");
        list1.Add("er wurde gehängt");
        list1.Add("er wurde ertränkt");
        _qqList.Add(new QuizQuestion(question, list1, 0));

        question = "Für welche der folgenden Lehren ist Sokrates bekannt?";
        var list2 = new List<string>();
        list2.Add("Syllogistik");
        list2.Add("Vier-Elemente-Lehre");
        list2.Add("Mäeutik");
        list2.Add("Ideenlehre");
        _qqList.Add(new QuizQuestion(question, list2, 2));

        question = "Welche Persönlichkeiten zählen zu den Schülern des Sokrates?";
        var list3 = new List<string>();
        list3.Add("Platon und Xenophon");
        list3.Add("Aristoteles");
        list3.Add("Augustinus von Hippo");
        list3.Add("Gaius Iulius Caesar");
        _qqList.Add(new QuizQuestion(question, list3, 0));

        question = "Aus welchem Material wurde die Büste des Sokrates ursprünglich hergestellt?";
        var list4 = new List<string>();
        list4.Add("Marmor");
        list4.Add("Bronze");
        list4.Add("Sandstein");
        list4.Add("Gold");
        _qqList.Add(new QuizQuestion(question, list4, 1));

    }

    private void UIHandler()
    {
        if (!_changeUI) return;

        if (_progress == SokratesProgress.Step1)
        {
            SwitchUITo(SOUNDBUTTONS);

        }else if (_progress == SokratesProgress.Step2)
        {
            SwitchUITo(SOKRATESZITATE);

        }else if (_progress == SokratesProgress.Step3)
        {
            SwitchUITo(STEP_3);
            _audioManager.SetAudioClip(AUDIOCLIP2); //hotfix davids soundfiles
        }
        else if (_progress == SokratesProgress.Step4)
        {
            SwitchUITo(STEP_4);
        }
        else if (_progress == SokratesProgress.Step5)
        {
            SwitchUITo(STEP_5);

            _quiz = true;
        }


        _changeUI = false;
    }

    public void SwitchUITo(Transform t)
    {
        var list = GetComponentsInChildren<Transform>(true).ToList().Where(tf => tf.tag == "UISection");
        foreach (var tr in list)
        {
            tr.gameObject.SetActive(false);
        }
        t.gameObject.SetActive(true);
    }

    private void CheckProgressConditions()
    {
        //begin sequence
        if (_trackableEventHandler.Tracked && _progress == SokratesProgress.Step0)
        {
            ProgressTo(1);
        }
        else if (_audioManager.Status == AudioStatus.Ended && _progress == SokratesProgress.Step1)
        {
            ProgressTo(2);
        }
        //progress to step 3 separate as button handled, pls have mercy. there was no time :(
    }

    public void ProgressTo(int p)
    {
        _progress = (SokratesProgress)p;
        _changeUI = true;
    }

    public void SoundButton()
    {
        if (_audioManager.Status == AudioStatus.Paused)
        {
            _audioManager.PlayAudio();
            SOUNDBUTTONS.GetComponentsInChildren<Transform>(true).Where(t => t.name == "Play").ToList()[0].gameObject.SetActive(false); //ehm
            SOUNDBUTTONS.GetComponentsInChildren<Transform>(true).Where(t => t.name == "Pause").ToList()[0].gameObject.SetActive(true);
        }
        else if(_audioManager.Status == AudioStatus.Playing)
        {
            _audioManager.PauseAudio();
            SOUNDBUTTONS.GetComponentsInChildren<Transform>(true).Where(t => t.name == "Play").ToList()[0].gameObject.SetActive(true);
            SOUNDBUTTONS.GetComponentsInChildren<Transform>(true).Where(t => t.name == "Pause").ToList()[0].gameObject.SetActive(false);
        }
    }

	public void SoundButton2() //i hate myself
    {
        if (_audioManager.Status == AudioStatus.Paused || _audioManager.Status == AudioStatus.Ended)
        {
            _audioManager.PlayAudio();
            STEP_3.GetComponentsInChildren<Transform>(true).Where(t => t.name == "Play").ToList()[0].gameObject.SetActive(false); //ehm
			STEP_3.GetComponentsInChildren<Transform>(true).Where(t => t.name == "Pause").ToList()[0].gameObject.SetActive(true);
        }
        else if(_audioManager.Status == AudioStatus.Playing)
        {
            _audioManager.PauseAudio();
			STEP_3.GetComponentsInChildren<Transform>(true).Where(t => t.name == "Play").ToList()[0].gameObject.SetActive(true);
			STEP_3.GetComponentsInChildren<Transform>(true).Where(t => t.name == "Pause").ToList()[0].gameObject.SetActive(false);
        }
    }

    public void QuizButtonAction(int index)
    {
        //correct answer
        if (index == _currentQuizQuestion.CorrectIndex)
        {
            EndQuiz(true);
        }
        else
        {
            EndQuiz(false);
        }
    }

    public void StartQuiz(QuizQuestion qq)
    {
        _currentQuizQuestion = qq;
        QUESTION_FIELD.GetComponent<Text>().text = qq.Question;
        A0.GetComponentInChildren<Text>().text = qq.Choices[0];
        A1.GetComponentInChildren<Text>().text = qq.Choices[1];
        A2.GetComponentInChildren<Text>().text = qq.Choices[2];
        A3.GetComponentInChildren<Text>().text = qq.Choices[3];
        QUIZ_TRANSFORM.gameObject.SetActive(true);
    }

    public void EndQuiz(bool won)
    {
        //QUIZ_TRANSFORM.gameObject.SetActive(false);
        //QUIZREWARD_TRANSFORM.gameObject.SetActive(true);
        //TODO add coin rewards
        if (won)
        {
            //QTRUE.gameObject.SetActive(true);
            //QFALSE.gameObject.SetActive(false);
            //_coins++;
            //COIN_NUMBER.GetComponent<Text>().text = _coins.ToString();
            _quizIndex++;
            _quizCont = true;
            var coin = Instantiate(CoinPrefab, COIN_NUMBER.position, Quaternion.identity);
            coin.transform.parent = STEP_5;
            coin.GetComponent<Coin>().Init(COIN_NUMBER.parent.position);

        }
        else
        {
            //TODO add something so you can actually lose this quiz : >
            //QFALSE.gameObject.SetActive(true);
            //QTRUE.gameObject.SetActive(false);
        }
    }

    public void AddCoin()
    {
        _coins++;
        COIN_NUMBER.GetComponent<Text>().text = _coins.ToString();
    }

    public void OpenHyperlink()
    {
        Application.OpenURL("http://www.zeit.de/wissen/2017-04/todesstrafe-hinrichtungen-wo-wie-warum-faq");
    }

    public void ChangeSpeechbubbleText()
    {
        SPEECHBUBBLETEXT.GetComponent<Text>().text = INPUT_FIELD.GetComponent<Text>().text;
    }
}

public class QuizQuestion
{
    public string Question;
    public List<string> Choices;
    public int CorrectIndex;

    public QuizQuestion(string question, List<string> choices, int correctIndex)
    {
        Question = question;
        Choices = choices;
        CorrectIndex = correctIndex;
    }
}



public enum SokratesProgress
{
    Step0,
    Step1,
    Step2,
    Step3,
    Step4,
    Step5
}
