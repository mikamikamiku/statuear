﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class ClickableStatue : MonoBehaviour
{
    private int _index;

    void OnMouseDown()
    {
        SceneManager.LoadScene(_index); //TODO insert correct scene index
    }

    public void Init(int index)
    {
        _index = index;
    }
}
