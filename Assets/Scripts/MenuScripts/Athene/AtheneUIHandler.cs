﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AtheneUIHandler : MonoBehaviour
{

    public Transform SOUNDBUTTONS;
    public AudioClip AUDIOCLIP;
    private AudioManager _audioManager;

    void Start(){
    	_audioManager = GetComponent<AudioManager>();
		_audioManager.SetAudioClip(AUDIOCLIP);
    }

    public void SwitchUITo(RectTransform transform)
    {

        //TODO Why the fuck is this not working
        //SOLVED: GetComponentsInChildren only returns active transforms; fix: GetComponentsInChildren<Transform>(true) !!! :D

        var list = GetComponentsInChildren<Transform>(true).ToList().Where(t => t.tag == "UISection");
        foreach (var t in list)
        {
            t.gameObject.SetActive(false);
        }

        transform.gameObject.SetActive(true);
    }

	public void SoundButton()
    {
        if (_audioManager.Status == AudioStatus.Paused)
        {
            _audioManager.PlayAudio();
            SOUNDBUTTONS.GetComponentsInChildren<Transform>(true).Where(t => t.name == "Play").ToList()[0].gameObject.SetActive(false); //ehm
            SOUNDBUTTONS.GetComponentsInChildren<Transform>(true).Where(t => t.name == "Pause").ToList()[0].gameObject.SetActive(true);
        }
        else if(_audioManager.Status == AudioStatus.Playing)
        {
            _audioManager.PauseAudio();
            SOUNDBUTTONS.GetComponentsInChildren<Transform>(true).Where(t => t.name == "Play").ToList()[0].gameObject.SetActive(true);
            SOUNDBUTTONS.GetComponentsInChildren<Transform>(true).Where(t => t.name == "Pause").ToList()[0].gameObject.SetActive(false);
        }
    }
}
