﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Event;
using UnityEngine;

public class AthenePainting : MonoBehaviour
{

    public Transform PAINTINGSTUFF;
    private TrackableEventHandler _tracker;

    void Start()
    {
        _tracker = GetComponent<TrackableEventHandler>();
    }

    void Update()
    {
        if (_tracker.Tracked)
        {
            PAINTINGSTUFF.gameObject.SetActive(true);
        }
        else
        {
            PAINTINGSTUFF.gameObject.SetActive(false);
        }
    }
}
