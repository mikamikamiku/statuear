﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuScript : MonoBehaviour
{

    public GameObject Sokrates;
    public GameObject Aspasia;
    public GameObject Athene;

    public Transform StatueAnchor;
    private List<Transform> _statueList;

    void Start()
    {
        // Athene spawn
        _statueList = new List<Transform>();
        _statueList.Add(Instantiate(Athene, StatueAnchor.position + Vector3.down, StatueAnchor.rotation).transform);
        Create3DText(_statueList[0].position + Vector3.up * 0.9f, "Athene \nParthenos", Vector3.up * 4.5f + Vector3.left * 0.8f);
        var clickable = _statueList[0].gameObject.AddComponent<ClickableStatue>();
        clickable.Init(2);


        //Sokrates spawn
        _statueList.Add(Instantiate(Sokrates, StatueAnchor.position + Vector3.left * 4.5f + Vector3.down * 3f, StatueAnchor.rotation).transform);
        Create3DText(_statueList[1].position + Vector3.up * 0.9f, "Sokrates", Vector3.down * 1.25f + Vector3.left * 0.8f);
        clickable = _statueList[1].gameObject.AddComponent<ClickableStatue>();
        clickable.Init(3);


        //Aspasia spawn
        _statueList.Add(Instantiate(Aspasia, StatueAnchor.position + Vector3.right * 4.5f + Vector3.down * 2f, StatueAnchor.rotation).transform);
        Create3DText(_statueList[2].position, "Aspasia", Vector3.down * 1.25f + Vector3.left * 0.8f);
        clickable = _statueList[2].gameObject.AddComponent<ClickableStatue>();
        clickable.Init(4);

    }
	
	void Update () {
	    foreach (var s in _statueList)
	    {
	        s.Rotate(Vector3.down, 10 * Time.deltaTime);
	    }
	}

    private void Create3DText(Vector3 pos, String text, Vector3 posMod)
    {
        var go = new GameObject(text + " :3DText");
        go.transform.position = pos + posMod;
        go.transform.rotation = Quaternion.identity;
        go.transform.localScale = Vector3.one * 0.04f;
        var t = go.AddComponent<TextMesh>();
        t.text = text;
        t.fontSize = 100; //good to know
    }
}
